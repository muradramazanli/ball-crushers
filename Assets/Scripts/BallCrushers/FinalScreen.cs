﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalScreen : MonoBehaviour
{
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        var balls = GameObject.FindObjectsOfType<BallCrusherBall>()
            .OrderBy(ball => ball.died).ToList();

        if (BallCrushers.Instance.gameType == GameType.FFA)
        {
            FreeForAll(balls);
        }
        else
        {
            TeamDeathMatch(balls);
        }

    }

    void FreeForAll(List<BallCrusherBall> balls)
    {
        var winner = balls.First();
        if (balls.Count(ball => ball.died == winner.died) > 1)
        {
            var winners = balls
            .Where(ball => ball.died == winner.died)
            .Select(ball => ball.name);

            text.text = string.Format("Winners are: {0}", string.Join(", ", winners));
        }
        else
        {
            text.text = string.Format("Winner is: {0}", winner.name);
        }
    }

    void TeamDeathMatch(List<BallCrusherBall> balls)
    {
        var blueTeamScore = balls
        .Where(ball => ball.ballTeam == Team.ENEMY)
        .Aggregate(0, (sum, ball) => sum + ball.died);

        var redTeamScore = balls
        .Where(ball => ball.ballTeam == Team.PLAYER)
        .Aggregate(0, (sum, ball) => sum + ball.died);
        if(blueTeamScore == redTeamScore){
            text.text="Draw";
            return;
        }

        text.text = string.Format("Winner is: {0}", blueTeamScore > redTeamScore ? "Blue Team" : "Read Team");
    }

    public void Reaload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
