﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cue : MonoBehaviour
{
    public int speed=100;
    void Start()
    {
        GetComponent<Rigidbody>().velocity=transform.forward*speed;
        Invoke("DestroyMe",2);
    }

    void DestroyMe(){
        Destroy(gameObject);
    }
}
