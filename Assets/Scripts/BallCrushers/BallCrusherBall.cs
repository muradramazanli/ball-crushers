﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum BallCrusherBallType
{
    PLAYER,
    AI

}

public enum Team
{
    PLAYER,
    ENEMY
}
public class BallCrusherBall : MonoBehaviour
{
    static List<Color> colors;

    static List<string> names;
    public GameObject cue;
    public BallCrusherBallType ballType;
    public float speed = 10;
    public int died = 0;
    public string ballName = "Player";
    public Team ballTeam;
    Rigidbody rb;
    LineRenderer lr;
    float timeToHit = 0;
    float nextShot;
    float y;
    Vector3 initialPosition;

    public static void SetStatics()
    {
        colors = new List<Color>(){
            Color.blue,
            Color.green,
            Color.red,
            Color.yellow,
            Color.cyan,
            Color.grey,
            Color.black,
            Color.magenta,
            new Color(0.2f,0,.4f,1),
            new Color(0.4f,0,.2f,1)
        };

        names = new List<string>(){
            "Loki","Odin","Thor","Freya","Tyr","Frigg","Heimdall","Baldr","Hel","Mimir"
        };
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (ballType == BallCrusherBallType.PLAYER)
        {
            lr = GetComponent<LineRenderer>();
        }
        BallCrushers.Instance.balls.Add(this);
        initialPosition = transform.position;
        if (ballType != BallCrusherBallType.PLAYER)
        {
            if (BallCrushers.Instance.gameType == GameType.FFA)
            {
                GetComponent<Renderer>().material.color = colors[0];
                colors.RemoveAt(0);
            }

            ballName = names[0];
            gameObject.name = ballName;
            names.RemoveAt(0);
            timeToHit = Random.Range(0f, 1.0f);
            gameObject.GetComponentInChildren<Text>().text = ballName;
        }
        nextShot = BallCrushers.Instance.nextShot;
    }
    void PlayerPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rch;

        if (Physics.Raycast(ray, out rch, 100))
        {
            Vector3 direction = rch.point - transform.position;
            Aim(direction, true);
        }
    }

    public void setTeam(Team team)
    {
        ballTeam = team;
        if (team == Team.PLAYER)
        {
            GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
    }

    void Aim(Vector3 direction, bool player = false)
    {
        direction.y = 0;
        RaycastHit rch;
        if (Physics.Raycast(transform.position, direction, out rch))
        {
            if (player)
            {
                lr.SetPosition(0, transform.position);
                lr.SetPosition(1, rch.point);
            }

            if (timeToHit == 0)
            {
                if (player)
                {
                    if (Input.GetMouseButton(0))
                    {
                        timeToHit = 1;
                        Instantiate(cue, transform.position + direction.normalized, Quaternion.LookRotation(direction));
                    }
                }
                if (!player)
                {
                    timeToHit = 1;
                    Instantiate(cue, transform.position + direction.normalized, Quaternion.LookRotation(direction));
                }
            }

        }
    }
    void AIPoint()
    {
        var balls = BallCrushers.Instance.balls
        .Where(ball =>
        {
            if (ball == null && ball == this)
            {
                return false;
            }
            if (BallCrushers.Instance.gameType == GameType.TDM)
            {
                return ball.ballTeam != ballTeam;
            }

            return true;
        })
        .OrderBy(ball =>
                {
                    return Vector3.Distance(transform.position, ball.transform.position);
                });

        foreach (var ball in balls)
        {
            var direction = ball.transform.position - transform.position;
            RaycastHit rch;
            if (Physics.Raycast(transform.position, ball.transform.position - transform.position, out rch))
            {
                if (rch.collider.GetComponent<BallCrusherBall>() == ball)
                {
                    Aim(direction);
                    break;
                }
            }
        }

    }

    void Update()
    {
        if (!BallCrushers.Instance.GameRunning)
        {
            rb.velocity = Vector3.zero;
            return;
        }

        if (timeToHit > 0)
        {
            timeToHit -= Time.deltaTime;
            if (timeToHit < 0)
            {
                timeToHit = 0;
            }
            if (ballType == BallCrusherBallType.PLAYER)
            {
                BallCrushers.Instance.SetSize(timeToHit);
            }
        }

        if (ballType == BallCrusherBallType.PLAYER)
        {
            PlayerPoint();
        }
        else
        {
            AIPoint();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (y == 0 && collision.collider.CompareTag("Table"))
        {
            y = transform.position.y;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Point"))
        {
            transform.position = initialPosition;
            died++;
        }
    }

    void LateUpdate()
    {
        if (y != 0)
        {
            transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, 0, y), transform.position.z);
        }

    }
}
