﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCMenuManager : MonoBehaviour
{
    public Dropdown gameType;
    public Dropdown duration;
    public Slider AIs;
    public Text size;

    public Toggle player;
    // Start is called before the first frame update
    void Start()
    {
        AIs.onValueChanged.AddListener(delegate
        {
            PlayerSizeChanged();
        });
    }
    public void PlayerSizeChanged()
    {
        size.text = AIs.value.ToString();
    }

    float getDuration()
    {
        switch (duration.value)
        {
            case 0: return 2 * 60;
            case 1: return 3 * 60;
            default:
                return 5 * 60;
        }
    }

    public void StartButton()
    {
        BallCrushers.Instance.StartGame((GameType)gameType.value, getDuration(), (int)AIs.value, player.isOn);
        gameObject.SetActive(false);
    }
}
