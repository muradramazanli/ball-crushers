﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameType
{
    FFA, // Free for ALL
    TDM // Team Death match
}
public class BallCrushers : MonoBehaviour
{
    public static BallCrushers Instance { get; set; }
    public List<BallCrusherBall> balls = new List<BallCrusherBall>();
    public Text CountScreenText;
    public bool gameHasStarted = false;
    public bool gameOver = false;
    public float nextShot = 1f;
    public Sprite[] cues = new Sprite[2];
    public int countToStart = 5;
    public GameType gameType;
    public GameObject AIBall;
    public GameObject gameStartUI;
    public GameObject finalScreen;
    public bool GameRunning
    {
        get { return gameHasStarted && !gameOver; }
    }
    public float gameDuration;
    public GameObject player;
    RectTransform imageContainer;
    Image cueReady;
    float imageContainerInitialSize;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        imageContainer = GameObject.Find("ImageContainer").GetComponent<RectTransform>();
        cueReady = GameObject.Find("CueReady").GetComponent<Image>();
        imageContainerInitialSize = imageContainer.sizeDelta.x;
        BallCrusherBall.SetStatics();
    }

    public void SetSize(float timeToHit)
    {
        if (timeToHit > 0)
        {
            if (cueReady.sprite != cues[1])
            {
                cueReady.sprite = cues[1];
            }
            Vector2 sizeDelta = imageContainer.sizeDelta;
            sizeDelta.x = imageContainerInitialSize - imageContainerInitialSize * (1 / nextShot) * timeToHit;

            imageContainer.sizeDelta = sizeDelta;

        }
        else
        {
            cueReady.sprite = cues[0];
        }
    }

    public void GameOver()
    {
        finalScreen.SetActive(true);
        gameOver = true;
    }

    public void StartGame(GameType gameType, float duration, int AIs, bool wantsToPlay)
    {
        player.SetActive(wantsToPlay);
        gameDuration = duration;
        this.gameType = gameType;
        putAIs(AIs);
        CountScreenText.gameObject.SetActive(true);
        StartCoroutine(CountToZero(countToStart));
    }

    void FreeForAll(List<Transform> spawnPoints, int size)
    {

        spawnPoints = spawnPoints.OrderBy(a => Guid.NewGuid()).ToList();
        for (int i = 0; i < size; i++)
        {
            Instantiate(AIBall, spawnPoints[i].position + Vector3.up, Quaternion.identity);
        }
    }
    void TeamDeathMatch(List<Transform> spawnPoints, int size)
    {
        var rightSpawn = spawnPoints.Where(a => a.CompareTag("RightSpawn")).ToList();
        var leftSpawn = spawnPoints.Where(a => a.CompareTag("LeftSpawn")).ToList();
        for (int i = 0; i < size; i++)
        {
            var team = i % 2 == 0 ? Team.ENEMY : Team.PLAYER;
            var position = team == Team.ENEMY ? leftSpawn[i / 2] : rightSpawn[i / 2];
            var ball = Instantiate(AIBall, position.position + Vector3.up, Quaternion.identity);
            ball.GetComponent<BallCrusherBall>().setTeam(team);
        }
    }
    void putAIs(int size)
    {

        var spawnPoints = GameObject.Find("SpawnPoints")
        .transform.GetComponentsInChildren<Transform>()
        .ToList()
        .Where(a => a.CompareTag("RightSpawn") || a.CompareTag("LeftSpawn"))
        .ToList();
        if (gameType == GameType.FFA)
        {
            FreeForAll(spawnPoints, size);
        }
        else
        {
            TeamDeathMatch(spawnPoints, size);
        }

    }

    IEnumerator CountToZero(int number)
    {
        number--;
        yield return new WaitForSeconds(1);

        if (number == 0)
        {
            gameHasStarted = true;
            CountScreenText.gameObject.SetActive(false);
            gameStartUI.SetActive(true);
        }
        else
        {
            CountScreenText.text = number.ToString();
            StartCoroutine(CountToZero(number));
        }
    }

}
