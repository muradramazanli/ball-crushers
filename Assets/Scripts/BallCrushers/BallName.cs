﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallName : MonoBehaviour
{
    float distance;
    // Start is called before the first frame update
    void Start()
    {
        distance = Vector3.Distance(transform.position, transform.parent.position);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = transform.parent.position + Vector3.up * distance;
        transform.LookAt(transform.parent.position);
    }
}
