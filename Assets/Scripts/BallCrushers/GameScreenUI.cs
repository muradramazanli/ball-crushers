﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreenUI : MonoBehaviour
{
    public Text time;
    public Transform leaderBoard;
    public GameObject leaderBoardItem;
    float totalTime;
    // Start is called before the first frame update
    void Start()
    {
        totalTime = BallCrushers.Instance.gameDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (BallCrushers.Instance.GameRunning)
        {
            time.text = CreateTimeText();
            totalTime -= Time.deltaTime;
            if (totalTime <= 0)
            {
                totalTime = 0;
                BallCrushers.Instance.GameOver();
            }
            foreach (Transform child in leaderBoard.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            var balls = GameObject.FindObjectsOfType<BallCrusherBall>()
            .OrderBy(ball => ball.died).ToList();

            foreach (var item in balls)
            {
                var lbi = Instantiate(leaderBoardItem) as GameObject;
                lbi.transform.SetParent(leaderBoard);
                var ballColor = item.GetComponent<Renderer>().material.color;
                lbi.GetComponent<Image>().color = new Color(ballColor.r, ballColor.g, ballColor.b, .4f);
                lbi.transform.GetChild(0).GetComponent<Text>().text = item.ballName;
                lbi.transform.GetChild(1).GetComponent<Text>().text = item.died.ToString();
            }

        }
    }

    string CreateTimeText()
    {
        int minutes = (int)totalTime / 60;
        int seconds = (int)totalTime % 60;

        return string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
    }
}
