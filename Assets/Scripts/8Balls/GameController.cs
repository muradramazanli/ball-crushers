﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public int power = 30;
    public bool player1 = true;
    public GameObject finishScreen;
    public Text finishScreenText;
    public BallType player1BallType = BallType.UNDEFINED;
    public BallType player2BallType = BallType.UNDEFINED;
    Slider slider;
    public bool gameOver = false;

    public bool moving = false;
    public bool pointed = false;

    public GameObject ghost;

    GameObject player;
    List<GameObject> balls;
    List<BallType> inPoint = new List<BallType>();

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        slider = GameObject.Find("Slider").GetComponent<Slider>();
        balls = GameObject.FindGameObjectsWithTag("Ball").ToList();
        player = GameObject.FindGameObjectWithTag("Player");
        balls.Add(player);
    }

    public void Point(BallType ballType)
    {
        inPoint.Add(ballType);
    }

    public void SetPower(System.Single value)
    {
        power = (int)value;
    }

    public void ResetValues()
    {
        float value = slider.maxValue / 2;
        power = (int)value;
        slider.value = value;
        inPoint.Clear();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (!gameOver)
        {
            if (moving)
            {
                StopMoving();
                return;
            }
            if (pointed)
            {
                PutItOnTable();
            }
        }

    }

    void PutItOnTable()
    {

        RaycastHit raycastHit;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Physics.Raycast(ray, out raycastHit, 100, 8)
        if (Physics.SphereCast(ray, 1, out raycastHit, 40f))
        {
            if (raycastHit.collider.CompareTag("Floor"))
            {
                ghost.SetActive(true);
                var position = raycastHit.point + Vector3.up;
                ghost.transform.position = position;
                if (Input.GetMouseButtonDown(0))
                {
                    pointed = false;
                    player.SetActive(true);
                    player.transform.position = position;
                }
            }
            else
            {
                ghost.SetActive(false);
            }
        }
    }

    void StopMoving()
    {
        bool allStoped = true;
        foreach (var ball in balls)
        {
            if (ball == null || ball.activeInHierarchy == false)
            {
                continue;
            }
            Rigidbody ballRigidBody;
            ball.TryGetComponent<Rigidbody>(out ballRigidBody);
            if (ballRigidBody == null)
            {
                continue;
            }
            float speed = ballRigidBody.velocity.magnitude;
            if (speed < .5)
            {
                ballRigidBody.velocity = Vector3.zero;
                ballRigidBody.angularVelocity = Vector3.zero;

            }
            else
            {
                allStoped = false;
                break;
            }
        }
        if (allStoped)
        {
            AfterStop();
            GameObject.Find("CurrentPlayer").GetComponent<Text>().text = string.Format("Turn: Player {0}", player1 ? 1 : 2);
            Dictionary<BallType, string> texts = new Dictionary<BallType, string>(){
                {BallType.SOLIDS,"Target: SOLIDS"},
                {BallType.STRIPES,"Target: STRIPES"},
            };
            if (player1BallType != BallType.UNDEFINED)
            {
                GameObject.Find("Target").GetComponent<Text>().text = player1 ? texts[player1BallType] : texts[player2BallType];
            }
        }
        moving = !allStoped;
    }

    void BallInUndefined(int solids, int stripes)
    {
        if (solids > 0 && stripes > 0)
        {
            return;
        }

        if (solids > 0)
        {
            player1BallType = player1 ? BallType.SOLIDS : BallType.STRIPES;
            player2BallType = player1 ? BallType.STRIPES : BallType.SOLIDS;
        }
        else if (stripes > 0)
        {
            player1BallType = player1 ? BallType.STRIPES : BallType.SOLIDS;
            player2BallType = player1 ? BallType.SOLIDS : BallType.STRIPES;
        }
        else
        {
            player1 = !player1;
        }
    }

    void Eight(int solids, int stripes, bool white)
    {
        gameOver = true;
        finishScreen.SetActive(true);
        var WinnerText = GameObject.Find("Winner").GetComponent<Text>();

        var StripesRemains = balls.Exists(ball =>
        {
            if (ball != null)
                return ball.GetComponent<Ball>().ballType == BallType.STRIPES;
            return false;
        });
        var SolidsRemains = balls.Exists(ball =>
        {
            if (ball != null)
                return ball.GetComponent<Ball>().ballType == BallType.SOLIDS;
            return false;
        });


        if (white || player1BallType == BallType.UNDEFINED)
        {
            WinnerText.text = string.Format("Player {0} Wins", player1 ? 2 : 1);
            return;
        }

        if (player1)
        {
            if (player1BallType == BallType.SOLIDS)
            {
                WinnerText.text = string.Format("Player {0} Wins", (SolidsRemains || solids > 0) ? 2 : 1);
            }
            else
            {
                WinnerText.text = string.Format("Player {0} Wins", (StripesRemains || stripes > 0) ? 2 : 1);
            }
        }
        else
        {
            if (player2BallType == BallType.SOLIDS)
            {
                WinnerText.text = string.Format("Player {0} Wins", (SolidsRemains || solids > 0) ? 1 : 2);
            }
            else
            {
                WinnerText.text = string.Format("Player {0} Wins", (StripesRemains || stripes > 0) ? 1 : 2);
            }
        }
    }
    void AfterStop()
    {
        int solids = inPoint.Count(a => a == BallType.SOLIDS);
        int stripes = inPoint.Count(a => a == BallType.STRIPES);
        bool eight = inPoint.Exists(a => a == BallType.EIGHT);
        bool white = inPoint.Exists(a => a == BallType.WHITE);

        if (eight)
        {
            Eight(solids, stripes, white);
            return;
        }

        if (player1BallType == BallType.UNDEFINED)
        {
            BallInUndefined(solids, stripes);
            return;
        }


        if (white)
        {
            player1 = !player1;
            return;
        }

        if (player1)
        {
            player1 = player1BallType == BallType.SOLIDS ? solids > 0 : stripes > 0;
        }
        else
        {
            player1 = !(player1BallType == BallType.STRIPES ? solids > 0 : stripes > 0);
        }

    }
}
