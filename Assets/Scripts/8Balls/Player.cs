﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 5;
    public LayerMask RayCastIgnoreLayer;
    public GameObject ghost;
    public float fixer = 1;
    public int force = 70;
    public bool pointed = false;
    float ballRadius;
    bool touched = false;
    Vector3 ballDestination = Vector3.zero;
    Rigidbody rb;
    LineRenderer lr;
    SphereCollider sc;

    Vector3 mainDirection;

    private void Start()
    {
        //lr = gameObject.AddComponent<LineRenderer>();
        lr = GetComponent<LineRenderer>();
        rb = GetComponent<Rigidbody>();
        sc = GetComponent<SphereCollider>();
        ballRadius = sc.radius * transform.localScale.x * fixer;
    }

    private void Update()
    {

        if (!GameController.Instance.moving && !GameController.Instance.gameOver && !GameController.Instance.pointed)
        {
            CreateLine();
        }
        else
        {
            ghost.SetActive(false);
            lr.enabled = false;
        }
    }


    void CreateLine()
    {
        RaycastHit raycastHit;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Physics.Raycast(ray, out raycastHit, 100, 8)
        if (Physics.Raycast(ray, out raycastHit, 40f))
        {
            lr.enabled = true;
            ghost.SetActive(true);
            Hit(raycastHit);
        }
        else
        {
            ghost.SetActive(false);
            lr.enabled = false;
        }
    }

    void Hit(RaycastHit raycastHit)
    {

        mainDirection = (raycastHit.point - transform.position).normalized;
        mainDirection.y = 0;
        
        if (!Physics.SphereCast(transform.position, ballRadius, mainDirection, out raycastHit,100,~10))
        {
            return;
        }

        //Physics.Raycast(transform.position, direction.normalized, out raycastHit, 150);
        if (raycastHit.transform.tag == "Table")
        {
            TableHit(raycastHit);
        }
        else
        {
            BallHit(raycastHit);
        }
        if (Input.GetMouseButtonDown(0))
        {
            rb.velocity = mainDirection * GameController.Instance.power;
            GameController.Instance.moving = true;
            GameController.Instance.ResetValues();
            touched = false;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        Direct(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        Direct(collision);
    }


    void Direct(Collision collision)
    {
        /*
         *  Here I am faking the direction as physics are changing the calculated direction
         *  I choose to direct it to a to a point myself
         */

        if (!touched && collision.collider.CompareTag("Ball") && ballDestination != Vector3.zero)
        {
            var crb = collision.collider.GetComponent<Rigidbody>();
            Vector3 ballDIrection = (ballDestination - collision.collider.transform.position).normalized;
            crb.velocity = ballDIrection * crb.velocity.magnitude;
            touched = true;
            ballDestination = Vector3.zero;
        }
    }

    private void BallHit(RaycastHit raycastHit)
    {
        ghost.transform.position = raycastHit.point + raycastHit.normal * ballRadius;
        List<Vector3> points = new List<Vector3>() { transform.position, raycastHit.point + raycastHit.normal * ballRadius };
        Physics.Raycast(raycastHit.transform.position, -raycastHit.normal, out raycastHit);

        ballDestination = raycastHit.point;

        points.Add(raycastHit.point);
        lr.positionCount = points.Count;
        lr.SetPositions(points.ToArray());
    }

    void TableHit(RaycastHit raycastHit)
    {
        List<Vector3> points = new List<Vector3>() { transform.position, raycastHit.point + raycastHit.normal };
        ghost.transform.position = raycastHit.point + raycastHit.normal * ballRadius;
        Vector3 newDirection = Vector3.Reflect(mainDirection, raycastHit.normal);

        if (Physics.Raycast(raycastHit.point, newDirection, out raycastHit, 150, ~RayCastIgnoreLayer))
            points.Add(raycastHit.point);


        lr.positionCount = points.Count;
        lr.SetPositions(points.ToArray());
    }

}
