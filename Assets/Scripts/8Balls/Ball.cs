﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BallType
{
    STRIPES,
    SOLIDS,
    WHITE,
    EIGHT,
    UNDEFINED
}
public class Ball : MonoBehaviour
{
    float y = 0;
    public BallType ballType;
    public Rigidbody rb;
    Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        transform.eulerAngles = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (y == 0 && collision.collider.CompareTag("Table"))
        {
            y = transform.position.y;
        }
    }


    //transform.Rotate(new Vector3(rb.velocity.z, 0, rb.velocity.x).normalized* Vector3.Distance(transform.position, position) * Time.deltaTime* 2 * Mathf.PI, Space.World);

    private void LateUpdate()
    {
        if (y != 0)
        {
            transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, 0, y), transform.position.z);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Point"))
        {
            GameController.Instance.Point(gameObject.GetComponent<Ball>().ballType);
            if (ballType == BallType.WHITE)
            {
                GameController.Instance.pointed = true;
                rb.velocity = Vector3.zero;
                gameObject.SetActive(false);
                return;
            }
            Destroy(gameObject);
        }
    }

}
